# FAIR data in Aspergillus research: The FAIR datastation? A solution for Aspergillus research?

For my master thesis with Mariana. In this work, I must promote the use of [FAIR](https://www.go-fair.org/fair-principles/) data practices among researchers in the *Aspergillus* field. I presented this talk at the [ASPAR_KR conference](https://www.aspar.website/).  This conference was held with the aim of furthering cooperation and data sharing in the *Aspergillus* field. In my talk, I promoted the usage of the [FAIR data station](https://data.m-unlock.nl/) to members of the audience. The FAIR data station is a webapp, that can use excel documents to generate [`rdf`](https://en.wikipedia.org/wiki/Resource_Description_Framework) files, a format that is central in the FAIR data paradigm for its interoperability.

The FAIR data station will be a part of the ASPAR_KR platform. In the platform, the FAIRDS will be used to standardise input data from users, so that it can be loaded into a graph database. The resulting database will then be usable for meta-analysis and data sharing.


## Slides

The presentation is hosted [here on bookdown](https://bookdown.org/sibbe_l_bakker/aspar_presentation/).

<iframe  width="500" height="1000px" title="slides"
src="https://bookdown.org/sibbe_l_bakker/aspar_presentation/"></iframe>
